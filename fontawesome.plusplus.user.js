// ==UserScript==
// @name        FontAwesome++
// @namespace   fontawesome
// @description Add ability to copy icons from font awesome /icon/* page
// @include     http://fontawesome.io/icon/*
// @version     1.0
// @grant       none
// ==/UserScript==

(function() {

	var isEnable = (function() {
		
		 return document.querySelector( '.info-class' ) != null;
	})();

	var fixer = new function() {
		var self = this;
		
		this.data = {};

		this.storeUnicode = function() {
			var el = document.querySelector( '.info-class .upper' );

			if (el == null)
				throw new Error( 'FontaAwesome++ storeUnicode() failed, el ".info-class .upper" not found' );
			else 
				this.data.unicode = '&#x' + el.innerHTML;
		};
		
		this.fixPreviewIcons = function() {
			var elPreviewIcons = document.querySelector( '.info-icons' );
			
			elPreviewIcons.innerHTML = (function() {
				var html = '';
				
				html += '<i class="fa fa-6">'+ self.data.unicode +'</i>';
				html += '<i class="fa fa-5">'+ self.data.unicode +'</i>';
				html += '<i class="fa fa-4">'+ self.data.unicode +'</i>';
				html += '<i class="fa fa-3">'+ self.data.unicode +'</i>';
				html += '<i class="fa fa-2">'+ self.data.unicode +'</i>';
				html += '<i class="fa fa-1">'+ self.data.unicode +'</i>';
				
				return html;
			})();
		};
		
		this.fixInfoIcon = function() {
			var elIcon = document.querySelector( '.info-class .fa' );
			
			if (elIcon == null) {
				console.log( 'FontaAwesome++ Warning fixInfoIcon() failed, el ".info-class fa" not found' );
			} else {
				elIcon.classList = 'fa';
				elIcon.innerHTML = this.data.unicode;
			}
		};
		
		this.fixWellIcon = function() {
			var elIcon = document.querySelector( '.well-transparent .fa' );
			
			if (elIcon == null) {
				console.log( 'FontaAwesome++ Warning fixWellIcon() failed, el ".well-transparent .fa" not found' );
			} else {
				elIcon.classList = 'fa';
				elIcon.innerHTML = this.data.unicode;
			}
		};
		
		this.init = function() {
			this.storeUnicode();
			this.fixPreviewIcons();
			this.fixInfoIcon();
			this.fixWellIcon();
		};
	};
	
	if (isEnable) fixer.init();
	
})();